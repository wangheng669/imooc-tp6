<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 15:06:01
 * @LastEditTime : 2020-02-12 23:12:18
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\provider.php
 */
use app\ExceptionHandle;
use app\Request;

// 容器Provider定义文件
return [
    'think\exception\Handle' => 'app\api\exception\Http'
];
