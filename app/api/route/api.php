<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 14:20:30
 * @LastEditTime: 2020-02-20 00:21:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\route\demo.php
 */

use think\facade\Route;

Route::rule('smscode','api/Sms/code','POST');
Route::rule('login','api/Login/index','POST');
Route::rule('category','api/Category/index','POST');
Route::rule('category/search/:id', 'api/Category/search','GET');
Route::rule('lists', 'api/mall.Lists/index','GET');
Route::rule('detail/:id', 'api/mall.Detail/index','GET');
Route::rule('subcategory/:id', 'api/Category/sub','GET');
Route::resource('user','User');
Route::resource('order','order.index');
Route::resource('logut','Logut');