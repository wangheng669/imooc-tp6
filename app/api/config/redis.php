<?php
/*
 * @Author: your name
 * @Date: 2020-02-12 23:07:19
 * @LastEditTime : 2020-02-12 23:09:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\config\redis.php
 */

return [

    'code_pre' => 'mall_code_pre_',
    'code_expire' => 50,
    'token_pre' => 'mall_token_pre_',
    'cart_pre' => 'mall_cart_pre_',
    'order_status_key' => 'order_status',
    'order_expire' => 20*60,
];