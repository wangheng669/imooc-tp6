<?php

namespace app\api\controller;

use app\common\lib\Show;

class Address extends AuthBase
{

    public function index()
    {
        $result = [
            [
                'id' => 1,
                'consignee_info' => '山东 济南 槐荫区 报业大厦 wangheng收 17615820146',
                'is_default' => 1,
            ]
        ];
        return Show::success($result);
    }

}