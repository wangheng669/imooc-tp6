<?php

namespace app\api\controller\order;

use app\api\controller\AuthBase;
use app\common\business\Order;
use app\common\lib\Show;

class Index extends AuthBase
{

    public function save()
    {
        // 获取参数
        $ids = input('param.ids');
        $address_id = input('param.address_id');
        $data = [
            'ids' => $ids,
            'address_id' => $address_id,
            'user_id' => $this->userId,
        ];
        try{
            $result = (new Order())->save($data);
        }catch(\Exception $e){
            return Show::error();
        }
        return Show::success($result);
    }


    public function read()
    {
        $orderId = input('param.id');
        $data = [
            'user_id' => $this->userId,
            'order_id' => $orderId
        ];
        $result = (new Order())->detail($data);
        return Show::success($result);
    }

}