<?php
/*
 * @Author: your name
 * @Date: 2020-02-16 22:15:38
 * @LastEditTime: 2020-02-16 22:15:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\controller\ApiBase.php
 */

namespace app\api\controller;

use app\BaseController;
use think\exception\HttpResponseException;

class ApiBase extends BaseController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function show(...$args)
    {
        throw new HttpResponseException(show(...$args));   
    }

}