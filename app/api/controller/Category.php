<?php
/*
 * @Author: your name
 * @Date: 2020-02-20 00:16:11
 * @LastEditTime: 2020-02-20 00:59:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\controller\Category.php
 */

namespace app\api\controller;

use app\common\business\Category as CategoryBis;
use app\common\lib\Arr;
use app\common\lib\Show;
use Exception;

class Category extends ApiBase{

    public function index()
    {
        try{
            $categoryBisObj = new CategoryBis();
            $categorys = $categoryBisObj->getNormalAllCategorys();
        }catch(Exception $e){
            return show(config('status.success'),'内部异常');
        }
        if(!$categorys){
            return show(config('status.error'),'数据为空');
        }
        $result = Arr::getTree($categorys);
        $result = Arr::sliceTreeArr($result);
        return show(config('status.success'),'OK',$result);
    }
    
    public function search()
    {
        $id = input('param.id');
        // 获取当前分类
        $nowCategory = (new CategoryBis())->getNormalById($id);
        if($nowCategory['pid'] == 0){
            $result = $nowCategory;
            // 找他的二级分类
            $result['list'][] = (new CategoryBis())->getNormalByPid($result['id']);
            $result['list'][] = (new CategoryBis())->getNormalByPid($result['list'][0][0]['id']);
            $result['focus_ids'] = [
                $result['list'][0][0]['id'],$result['list'][1][0]['id'],
            ];
        }else{
            // 得到父级分类
            $result = (new CategoryBis())->getNormalById($nowCategory['pid']);
            $result['focus_ids'] = [
                $nowCategory['pid'],(int)$id,
            ];
            // 得到相同的二级分类
            $result['list'][] = (new CategoryBis())->getNormalByPid($result['pid']);
            $result['list'][] = (new CategoryBis())->getNormalByPid($nowCategory['pid']);
        }
        
        return show(config('status.success'),'OK',$result);
    }

    public function sub()
    {
        $id = input('param.id');
        $result = (new CategoryBis())->getNormalByPid($id);
        return show(config('status.success'),'OK',$result);
    }

}