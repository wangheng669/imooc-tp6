<?php

namespace app\api\controller;

use app\common\business\Cart as CartBis;
use app\common\lib\Show;

class Cart extends AuthBase
{

    public function add()
    {
        if(!$this->request->isPost()){
            return Show::error();
        }
        $id = input('param.id',0,'intval');
        $num = input('param.num',0,'intval');
        if(!$id || !$num){
            return Show::error('参数不合法!');
        }
        $result = (new CartBis())->insertRedis($this->userId,$id,$num);
        if($result){
            return Show::error();
        }
        return Show::success();
    }

    public function lists()
    {
        $ids = input('param.id');
        $result = (new CartBis())->lists($this->userId,$ids);
        return Show::success($result);
    }

    public function update()
    {
        $id = input('param.id');
        $num = input('param.num');
        try{
            $result = (new CartBis())->updateRedis($this->userId,$id,$num);
            if($result === FALSE){
                return Show::error();
            }
        
        }catch(\Exception $e){
            return Show::success($e->getMessage());
        }
        return Show::success();
    }

    public function delete()
    {
        $id = input('param.id');
        $result = (new CartBis())->deleteRedis($this->userId,$id);
        if($result === FALSE){
            return Show::error();
        }
        return Show::success();
    }

}