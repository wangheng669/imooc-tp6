<?php
/*
 * @Author: your name
 * @Date: 2020-02-16 22:15:38
 * @LastEditTime: 2020-02-16 22:15:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\controller\ApiBase.php
 */

namespace app\api\controller;

class AuthBase extends ApiBase{

    public $userId = '';
    public $userName = '';
    public $accessToken = '';
    public $isLogin = 0;
    
    // 初始化校验是否登录
    public function initialize()
    {
        parent::initialize();
        // if($this->isLogin == 1){
            $this->userId = 3;
            return true;
        // }
        $this->accessToken = $this->request->header('access-token');
        if(!$this->accessToken || !$this->isLogin()){
            return $this->show(config('status.not_login'),'没有登录');
        }
    }

    // 是否登录
    public function isLogin()
    {
        $userInfo = cache(config('redis.token_pre').$this->accessToken);
        if(!$userInfo){
            return false;
        }
        if(!empty($userInfo['id']) && !empty($userInfo['username'])){
            $this->userId = $userInfo['id'];
            $this->userName = $userInfo['username'];
            return true;
        }
        return false;
    }


}