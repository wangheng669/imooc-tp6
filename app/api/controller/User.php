<?php
/*
 * @Author: your name
 * @Date: 2020-02-16 22:35:24
 * @LastEditTime: 2020-02-16 22:35:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\api\controller\User.php
 */

namespace app\api\controller;

use app\api\validate\UserValidate;
use app\common\business\User as UserBis;

class User extends AuthBase
{
    public function index()
    {
        $user = (new UserBis())->getNormalUserByUserId($this->userId);
        $resUser = [
            'id' => $this->userId,
            'username' => $user['username'],
            'sex' => $user['sex'],
        ];
        return show(config('status.success'),'OK',$resUser);
    }

    public function update()
    {
        $username = input('param.username','','trim');
        $sex = input('param.sex',0,'intval');
        $data = [
            'username' => $username, 
            'sex' => $sex, 
        ];
        
        $validate = new UserValidate();
        if(!$validate->scene('update_user')->check($data)){
            return show(config('status.error'),$validate->getError());
        }

        // 更新数据
        $user = (new UserBis())->update($this->userId,$data,$this->accessToken);
        if(!$user){
            return show(config('status.error'),'更新失败');
        }
        return show(1,'ok');
    }

}