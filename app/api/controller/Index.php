<?php

namespace app\api\controller;

use app\common\business\Goods as GoodsBis;
use app\common\business\Category as CategoryBis;
use app\common\lib\Show;
use think\facade\Cache;

class Index extends ApiBase
{

    public function demo()
    {
        $result = Cache::zRangeByScore('order_status',0,time(),['limit' => [0,10]]);
        $snowflake = new \Godruoyi\Snowflake\Snowflake;
        $result = $snowflake->id();
        dd($result);
    }

    public function getRotationChart()
    {
        $result = (new GoodsBis())->getRotationChart();
        return Show::success($result);
    }

    public function categoryGoodsRecommend()
    {
        $categoryIds = (new CategoryBis())->getRecommendCategory();
        $result = (new GoodsBis())->categoryGoodsRecommend($categoryIds);
        return Show::success($result);
    }

}