<?php

namespace app\api\controller\mall;

use app\api\controller\ApiBase;
use app\common\business\GoodsSku as GoodsSkuBis;
use app\common\business\SpecsValue as SpecsValueBis;
use app\common\lib\Show;
use think\facade\Cache;

class Detail extends ApiBase
{

    public function index()
    {
        $id = input('param.id');
        $goodsSku = (new GoodsSkuBis())->getNormalSkuAndGoods($id);
        $goods = $goodsSku['goods'];
        // 获取当前商品的所有SKU
        $skus = (new GoodsSkuBis())->getSkuByGoodsId($goods['id']);
        // 获取当前默认sku
        foreach($skus as $sv){
            if($sv['id'] == $id){
                $flagValue = $sv['specs_value_ids'];
            }
        }
        $gids = array_column($skus,'id','specs_value_ids');
        if($goods['goods_specs_type'] == 1){
            $sku = [];
        }else{
            $sku = (new SpecsValueBis())->dealGoodsSkus($gids,$flagValue);
        }
        $result = [
            "title" => $goods['title'],
            "price" => $goodsSku['price'],
            "cost_price" => $goodsSku['cost_price'],
            "sales_count" => 0,
            "stock" => $goodsSku['stock'],
            "gids" => $gids,
            "image" => $goods['carousel_image'],
            "sku" => $sku,
            "detail" => [
                "d1" => [
                    "商品编码" => $goodsSku['id'],
                    "上架时间" => $goods['create_time'],
                ],
                "d2" => preg_replace('/(<img.+?src=")(.*?)/', '$1'.request()->domain().'$2',$goods['description']),
            ],
        ];
        Cache::inc('mall_pv_'.$goods['id']);
        return Show::success($result);
    }

}