<?php

namespace app\api\controller\mall;

use app\api\controller\AuthBase;
use app\common\business\Cart;
use app\common\lib\Show;

class Init extends AuthBase
{

    public function index()
    {
        $result = (new Cart())->lengthRedis($this->userId);
        $result = [
            'cart_num' => $result,
        ];
        return Show::success($result);
    }

}