<?php

namespace app\api\controller\mall;

use app\common\business\Goods as GoodsBis;
use app\api\controller\ApiBase;
use app\common\lib\Show;

class Lists extends ApiBase
{

    public function index()
    {
        $pageSize = input('param.page_size',10,'intval');
        $categoryId = input('param.category_id',0,'intval');
        if($categoryId){
            $data = [
                'category_path_id' => $categoryId,
            ];
        }
        $title = input('param.keyword'); 
        if($title){
            $data['title'] = $title;
        }
        $field = input('param.field','listorder','trim');
        $order = input('param.order',2,'intval');
        $order = $order == 2 ? 'desc' : 'asc';
        $listorder = [$field => $order];
        $goods = (new GoodsBis())->getNormalLists($data,$pageSize,$listorder);
        return Show::success($goods);
    }

}