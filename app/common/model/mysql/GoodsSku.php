<?php

namespace app\common\model\mysql;

class GoodsSku extends BaseModel
{

    public function goods()
    {
        return $this->hasOne(Goods::class,'id','goods_id');
    }

    public function getSkuByGoodsId($id,$field)
    {
        $where = [
            'goods_id' => $id,
            'status' => config('status.mysql.table_normal'),
        ];
        $listorder = [
            'id' => 'desc',
        ];
        return $this->where($where)->order($listorder)->field($field)->select();
    }

    public function decStock($id,$num)
    {
        return $this->where(['id' => $id])->dec('stock',$num)->update();
    }

}