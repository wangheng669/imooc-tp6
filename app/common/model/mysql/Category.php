<?php
/*
 * @Author: your name
 * @Date: 2020-02-01 22:55:09
 * @LastEditTime: 2020-02-20 00:20:15
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\model\mysql\Test.php
 */

namespace app\common\model\mysql;

class Category extends BaseModel
{

    // 获取分类列表
    public function getNormalCategorys($field)
    {
        $where = [
            'status' => config('status.mysql.table_normal'),
        ];
        $order = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        return $this->where($where)->field($field)->order($order)->select();
    }

    // 获取分页分类列表
    public function getLists($where,$num)
    {
        $order = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        $result = $this->where('status','<>',config('status.mysql.table_delete'))
        ->where($where)->order($order)->paginate($num);
        return $result;
    }

    // 根据PID获取子类
    public function getChildCountInPids($pids)
    {
        $where[] = ['pid','in',$pids];
        $where[] = ['status','<>',config('status.mysql.table_delete')];
        $result = $this->where($where)
        ->field(['pid','count(*) as count'])
        ->group('pid')
        ->select();
        return $result;
    }

    // 根据PID获取分类
    public function getNormalByPid($pid,$field)
    {
        $where = [
            'pid' => $pid,
            'status' => config('status.mysql.table_normal'),
        ];
        $listorder = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        return $this->where($where)->order($listorder)->field($field)->select();
    }

    // 根据id获取单个分类
    public function getNormalById($id,$field)
    {
        $where = [
            'id' => $id,
            'status' => config('status.mysql.table_normal'),
        ];
        return $this->where($where)->field($field)->find();
    }

    

}