<?php
/*
 * @Author: your name
 * @Date: 2020-02-03 22:12:21
 * @LastEditTime : 2020-02-04 21:22:57
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\model\mysql\AdminUser.php
 */

namespace app\common\model\mysql;

use think\Model;

class BaseModel extends Model
{
    
    protected $autoWriteTimestamp = true;

    // 根据主键更新数据
    public function updateById($id,$data)
    {
        $id = intval($id);
        if(empty($id) || empty($data) || !is_array($data)){
            return [];
        }
        $where = [
            'id' => $id,
        ];
        $data['update_time'] = time();
        $result = $this->where($where)->update($data);
        return $result;
    }


    public function getNormalByIds($ids)
    {
        $result = $this->whereIn('id',$ids)->select();
        return $result;
    }

    // 根据查询条件获取数据
    public function getByCondition($condition,$order = [])
    {
        $order = [
            'id' => 'desc'
        ];
        return $this->where($condition)->order($order)->select();
    }


}