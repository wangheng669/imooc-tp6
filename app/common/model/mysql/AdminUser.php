<?php
/*
 * @Author: your name
 * @Date: 2020-02-03 22:12:21
 * @LastEditTime : 2020-02-04 21:22:57
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\model\mysql\AdminUser.php
 */

namespace app\common\model\mysql;

class AdminUser extends BaseModel
{

    public function getAdminUserByUsername($username)
    {
        if(empty($username)){
            return [];
        }
        
        $where = [
            'username' => $username,
        ];

        $result = $this->where($where)->find();
        return $result;
    }

    
}
