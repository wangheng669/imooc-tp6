<?php
/*
 * @Author: your name
 * @Date: 2020-02-03 22:12:21
 * @LastEditTime : 2020-02-04 21:22:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\model\mysql\AdminUser.php
 */

namespace app\common\model\mysql;

class User extends BaseModel
{

    // 根据ID获取用户信息
    public function getUserById($id)
    {
        $id = intval($id);
        if(!$id){
            return false;
        }
        return $this->find($id);
    }


    // 根据手机号获取用户信息
    public function getUserByPhoneNumber($phoneNumber)
    {
        if(empty($phoneNumber)){
            return [];
        }
        
        $where = [
            'username' => $phoneNumber,
        ];

        $result = $this->where($where)->find();
        return $result;
    }

    // 根据昵称获取用户信息
    public function getUserByUserName($userName)
    {
        if(empty($userName)){
            return [];
        }
        
        $where = [
            'username' => $userName,
        ];

        $result = $this->where($where)->find();
        return $result;
    }
    
}
