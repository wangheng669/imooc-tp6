<?php

namespace app\common\model\mysql;

class SpecsValue extends BaseModel
{

    public function getNormalInIds($ids)
    {
        return $this->whereIn('id',$ids)
        ->where('status','=',config('status.mysql.table_normal'))
        ->select();
    }


}