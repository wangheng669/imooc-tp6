<?php

namespace app\common\model\mysql;

class Goods extends BaseModel
{
    
    // 图片转换
    public function getCarouselImageAttr($value)
    {
        if(!empty($value)){
            $value = explode(',', $value);
            $value = array_map(function($v){
                return request()->domain().$v;
            },$value);
        }
        return $value;
    }

    // 搜索title
    public function searchTitleAttr($query,$value)
    {
        $query->where('title','like',"%$value%");
    }

    // 搜索时间
    public function searchTimeAttr($query,$value)
    {
        $query->whereBetweenTime('create_time',$value[0],$value[1]);
    }

    public function getLists($data,$num)
    {
        if($data){
            // 获取key
            $searchKey = array_keys($data);
            $res = $this->withSearch($searchKey,$data);
        }else{
            $res = $this;
        }
        $order = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        $list = $res->whereIn('status',[0,1])->order($order)->paginate($num,false,['query' => request()->param()]);
        return $list;
    }

    public function getNormalGoodsByCondition($where,$field = true,$limit = 5)
    {
        $listorder = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        $where['status'] = config('status.success');
        return $this->limit($limit)->where($where)->order($listorder)->field($field)->select();
    }

    // 获取分类下的商品
    public function getNormalGoodsFindInSetCategoryId($categoryId,$field,$limit = 5)
    {
        $listorder = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        return $this->whereFindInset('category_path_id',$categoryId)->where([
            'status' => config('status.success'),
        ])->order($listorder)->limit($limit)->field($field)->select();

    }

    public function getImageAttr($value)
    {
        return request()->domain().$value;
    }

    public function getNormalGoodsById($id)
    {
        return $this->where([
            'id' => $id,
            'status' => config('status.success'),
        ])->find();
    }

    public function getNormalLists($data,$num,$field,$listorder = [])
    {
        $res = $this;
        if(isset($data['title'])){
            $searchKey = array_keys($data);
            $res = $this->withSearch($searchKey,$data);
        }
        $order = [
            'listorder' => 'desc',
            'id' => 'desc',
        ];
        if(isset($listorder)){
          $listorder = $order;  
        }
        $where['status'] = config('status.success');
        if(isset($data['category_path_id'])){
            $res = $this->whereFindInSet('category_path_id', $data['category_path_id']);
        }
        $list = $res->where($where)
        ->order($order)
        ->field($field)
        ->paginate($num,false,['query' => request()->param()]);
        return $list;
    }

}