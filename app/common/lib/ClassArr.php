<?php
/*
 * @Author: your name
 * @Date: 2020-02-12 23:26:36
 * @LastEditTime : 2020-02-13 00:58:10
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\lib\Num.php
 */

namespace app\common\lib;

class ClassArr{

    public static function smsClassStat()
    {
        return [
            'ali' => 'app\common\lib\sms\AliSms',
            'bd' => 'app\common\lib\sms\BdSms',
            'jd' => 'app\common\lib\sms\JdSms',
        ];
    }

    public static function uploadClassStat()
    {
        return [
        ];
    }

    public static function initClass($type,$class,$params = [],$needInstance = false)
    {
        if(!array_key_exists($type,$class)){
            return false;
        }
        $className = $class[$type];

        return $needInstance == true ? (new \ReflectionClass($className))->newInstanceArgs($params) : $className;

    }
    
}