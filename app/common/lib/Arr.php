<?php
/*
 * @Author: your name
 * @Date: 2020-02-20 00:22:43
 * @LastEditTime: 2020-02-20 01:00:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\lib\Arr.php
 */

namespace app\common\lib;

class Arr{

    public static function getTree($data)
    {
        $items = [];
        foreach($data as $v){
            $items[$v['category_id']] = $v;
        }
        $tree = [];
        foreach($items as $id => $item){
            if(isset($items[$item['pid']])){
                $items[$item['pid']]['list'][] = &$items[$id];
            }else{
                $tree[] = &$items[$id];
            }
        }
        return $tree;
    }

    public function getPaginateDefaultData($num = 5)
    {
        $result = [
            'total' => 0,
            'per_page' => $num,
            'current_page' => 1,
            'last_page' => 0,
            'data' => [],
        ];
        return $result;
    }

    // 截取分类
    public static function sliceTreeArr($data, $firstCount = 5, $secondCount = 3, $treeCount = 5)
    {
        $data = array_slice($data, 0, $firstCount);
        foreach($data as $k => $v){
            if(!empty($v['list'])){
                $data[$k]['list'] = array_slice($v['list'],0,$secondCount);
                foreach($v['list'] as $kk => $vv){
                    if(!empty($vv['list'])){
                        $data[$k]['list'][$kk]['list'] = array_slice($vv['list'],0,$treeCount);
                    }
                }
            }
        }
        return $data;
    }

    // 数组排序
    public static function arrsSortByKey($result,$key,$sort = SORT_DESC)
    {
        array_multisort(array_column($result,$key),$sort,$result);
        return $result;
    }

}