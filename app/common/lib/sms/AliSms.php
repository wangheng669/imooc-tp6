<?php
/*
 * @Author: your name
 * @Date: 2020-02-05 21:59:36
 * @LastEditTime : 2020-02-13 01:01:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\lib\sms\AliSms.php
 */

namespace app\common\lib\sms;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use think\facade\Log;

class AliSms implements BaseSms{

    public static function sendCode($phoneNumbers, $code)
    {

        if(empty($phoneNumbers) || empty($code)){
            return false;
        }
        $TemplateParam = [
            'code' => $code,
        ];
        AlibabaCloud::accessKeyClient(config('aliyun.access_key_id'), config('aliyun.access_secret'))
        ->regionId(config('aliyun.region_id'))
        ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
            ->product('Dysmsapi')
            // ->scheme('https') // https | http
            ->version('2017-05-25')
            ->action('SendSms')
            ->method('POST')
            ->host('dysmsapi.aliyuncs.com')
            ->options([
                'query' => [
                'RegionId' => config('aliyun.region_id'),
                'PhoneNumbers' => $phoneNumbers,
                'SignName' => config('aliyun.sign_name'),
                'TemplateCode' => "SMS_183267606",
                'TemplateParam' => json_encode($TemplateParam),
                ],
            ])
            ->request();
            // print_r($result->toArray());
            Log::info("alisms-sendCode-{$phoneNumbers}-result",$result->toArray());
        } catch (ClientException $e) {
            // Log::info("alisms-sendCode-{$phoneNumbers}-ClientException",json_encode($e->getErrorMessage()));
            return true;
            // echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            // Log::info("alisms-sendCode-{$phoneNumbers}-ServerException",json_encode($e->getErrorMessage()));
            return true;
            // echo $e->getErrorMessage() . PHP_EOL;
        }

        if(isset($result['Code']) && $result['Code'] == 'OK'){
            return true;
        }
        return false;
    }
    
}