<?php

namespace app\common\lib;

class Show
{

    public static function success($data = [],$message = 'OK')
    {
        $result = [
            'status' => config('status.success'),
            'message' => $message,
            'result' => $data,
        ];
        return json($result);
    }

    public static function error($message = 'OK',$data = [],$status = 0)
    {
        $result = [
            'status' => $status,
            'message' => $message,
            'result' => $data,
        ];
        return json($result);
    }

}