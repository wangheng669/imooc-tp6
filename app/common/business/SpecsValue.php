<?php

namespace app\common\business;

use app\common\model\mysql\SpecsValue as SpecsValueModel;

class SpecsValue extends BusBase
{

    protected $model = null;

    public function __construct()
    {
        $this->model = new SpecsValueModel();
    }

    // 获取属性列表
    public function getBySpecsId($specsId)
    {
        $where = [
            'specs_id' => $specsId,
            'status' => config('status.mysql.table_normal')
        ];
        try{
            $res = $this->model->where($where)->select();
        }catch(\Exception $e){
            return [];                                                                                                                                                                                                     
        }
        return $res->toArray();
    }

    // 删除属性
    public function delSpecs($specsId)
    {
        $where = [
            'id' => $specsId,
        ];
        $res = $this->model->where($where)->update([
            'status' => config('status.mysql.table_delete')
        ]);
        return $res;
    }

    // 获取属性供前端展示
    public function dealGoodsSkus($gids,$flagValue)
    {
        $specsValuekeys = array_keys($gids);
        foreach($specsValuekeys as $specsValuekey){
            $specsValuekey = explode(',',$specsValuekey);
            foreach($specsValuekey as $k => $v){
                $new[$k][] = $v;
                $specsValueIds[] = $v;
            }
        }
        $result = [];
        $specsValueIds = array_unique($specsValueIds);
        $specsValues = $this->getNormalInIds($specsValueIds);
        $flagValue = explode(',',$flagValue);
        foreach($new as $key => $newValue){
            $newValue = array_unique($newValue);
            $list = [];
            foreach($newValue as $vv){
                $list[] = [
                    'id' => $vv,
                    'name' => $specsValues[$vv]['name'],
                    'flag' => in_array($vv,$flagValue),
                ];
            }
            $result[$key] = [
                'name' => $specsValues[$newValue[0]]['specs_name'],
                'list' => $list,
            ];

        }
        return $result;
    }

    public function dealSpecsValue($skuIdSpecsValueIds)
    {
        // 格式化属性值用于购物车
        // 拿到规格属性ID
        $ids = array_values($skuIdSpecsValueIds);
        $ids = implode(',',$ids);
        $ids = array_unique(explode(',',$ids));
        // 获取规格属性
        $result = $this->getNormalInIds($ids);
        if(!$result){
            return [];
        }
        $res = [];
        foreach($skuIdSpecsValueIds as $skuId => $specs){
            if(!$specs){
                continue;
            }
            $specs = explode(',',$specs);
            $skuStr = [];
            foreach($specs as $spec){
                $skuStr[] = $result[$spec]['specs_name'].':'.$result[$spec]['name'];
            }
            $res[$skuId] = implode('  ',$skuStr);
        }
        return $res;
    }

    public function getNormalInIds($ids)
    {
        try{
            $result = $this->model->getNormalInIds($ids);
        }catch(\Exception $e){
            return [];
        }
        $result = $result->toArray();
        if(!$result){
            return [];
        }
        $specsName = config('specs');
        $specsNameArrs = array_column($specsName,'name','id');
        $res = [];
        foreach($result as $resultValue){
            $res[$resultValue['id']] = [
                'name' => $resultValue['name'],
                'specs_name' => $specsNameArrs[$resultValue['specs_id']],
            ];
        }
        return $res;
    }

}