<?php

namespace app\common\business;

use think\facade\Cache;
use app\common\lib\Key;
use app\common\lib\Arr;
use app\common\business\GoodsSku;
use app\common\business\SpecsValue;
use Exception;

class Cart extends BusBase
{

    public function insertRedis($userId,$id,$num)
    {
        $goodsSku = (new GoodsSku())->getNormalSkuAndGoods($id);
        if(!$goodsSku){
            // 因为redis插入失败会返回大写的false
            return FALSE;
        }
        $data = [
            'title' => $goodsSku['goods']['title'],
            'image' => $goodsSku['goods']['recommend_image'],
            'num' => $num,
            'goods_id' => $goodsSku['goods']['id'],
            'create_time' => time(),
        ];
        $get = Cache::hGet(Key::userCart($userId),$id);
        if($get){
            $getData = json_decode($get,true);
            $data['num'] += $getData['num'];
        }
        $result = Cache::hSet(Key::userCart($userId),$id,json_encode($data));
        return $result;
    }

    public function lists($userId,$ids)
    {

        // 获取指定商品
        if($ids){
            $ids = explode(',',$ids);
            $res = Cache::hMget(Key::userCart($userId),$ids);
            if(in_array(false,array_values($res))){
                return [];
            }
        }else{
            $res = Cache::hGetAll(Key::userCart($userId));
        }
        // 获取sku
        $skuIds = array_keys($res);
        $skus = (new GoodsSku())->getNormalSkuByIds($skuIds);
        // 获取商品库存
        $stocks = array_column($skus,'stock','id');
        $skuIdPrice = array_column($skus,'price','id');
        $skuIdSpecsValueIds = array_column($skus,'specs_value_ids','id');
        $specsValues = (new SpecsValue())->dealSpecsValue($skuIdSpecsValueIds);
        foreach($res as $k => $v){
            $price = $skuIdPrice[$k] ?? 0;
            $v = json_decode($v,true);
            // 判断是否存在库存
            if($ids && isset($stocks[$k]) && $stocks[$k] < $v['num']){
                throw new \Exception($v['title'].'库存不足');
            }
            $v['id'] = $k;
            $v['image'] = preg_match('/http:\/\//',$v['image'])?$v['image']:request()->domain().$v['image'];
            $v['price'] = $price;
            $v['total_price'] = $price * $v['num'];
            $v['sku'] = $specsValues[$k] ?? '暂无规格';
            $result[] = $v;
        }
        if($result){
            // 排序
            $result = Arr::arrsSortByKey($result,'create_time');
        }
        return $result;
    }

    public function updateRedis($userId,$id,$num)
    {
        // 获取数据
        $get = Cache::hGet(Key::userCart($userId),$id);
        if($get){
            $getData = json_decode($get,true);
            $getData['num'] = $num;
        }else{
            throw new Exception('不存在该商品');
        }
        $result = Cache::hSet(Key::userCart($userId),$id,json_encode($getData));
        return $result;
    }

    public function deleteRedis($userId,$ids)
    {
        if(!is_array($ids)){
            $ids = explode(',',$ids);
        }
        $result = Cache::hDel(Key::userCart($userId),...$ids);
        return $result;
    }

    public function lengthRedis($userId)
    {
        try{
            $result = Cache::hLen(Key::userCart($userId));
        }catch(\Exception $e){
            return 0;
        }
        return intval($result);
    }

}