<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 23:57:41
 * @LastEditTime: 2020-02-20 00:27:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\business\AdminUser.php
 */

namespace app\common\business;

use app\common\model\mysql\Category as CategoryModel;
use Exception;

class Category{

    protected $model = null;

    public function __construct()
    {
        $this->model = new CategoryModel();
    }

    public function add($data)
    {
        $data['status'] = config('status.mysql.table_normal');
        $name = $data['name'];
        $result = $this->model->where(['name' => $name])->find();
        if($result){
            throw new Exception('已存在分类');
        }
        try{
            $this->model->save($data);
        }catch(Exception $e){
            throw new Exception('服务器内部错误');
        }
        return $this->model->id;
    }

    // 根据条件获取分类列表
    public function getLists($data,$num)
    {
        $list = $this->model->getLists($data,$num);
        if(!$list){
            return [];
        }
        $result = $list->toArray();
        $result['render'] = $list->render();
        $pids = array_column($result['data'],'id');
        if($pids){
            $idCountResult = $this->model->getChildCountInPids($pids);
            $idCountResult = $idCountResult->toArray();

            $idCounts = [];
            foreach($idCountResult as $countResult){
                $idCounts['pid'] = $countResult['count'];
            }
        }

        if($result['data']){
            foreach($result['data'] as $k => $value){
                $result['data'][$k]['childCount'] = $idCounts[$value['id']] ?? 0;
            }
        }
        

        return $result;
        
    }

    // 获取分类列表
    public function getNormalCategorys()
    {
        $field = 'id,name,pid';
        $categorys = $this->model->getNormalCategorys($field);
        if(!$categorys){
            return [];
        }
        return $categorys->toArray();
    }

    // 获取分类列表
    public function getNormalAllCategorys()
    {
        $field = 'id as category_id,name,pid';
        $categorys = $this->model->getNormalCategorys($field);
        if(!$categorys){
            return [];
        }
        return $categorys->toArray();
    }

    // 排序
    public function listorder($id,$listorder)
    {
        $result = $this->getById($id);
        if(!$result){
            throw new Exception('不存在该记录');
        }
        $data = [
            'listorder' => $listorder,
        ];
        try{
            $result = $this->model->updateById($id,$data);
        }catch(Exception $e){
            return false;
        }
        return $result;
    }

    // 根据ID获取数据
    public function getById($id)
    {
        $result = $this->model->find($id);
        if(empty($result)){
            return [];
        }
        return $result->toArray();
    }

    // 更新状态
    public function status($id,$status,$type)
    {
        $result = $this->getById($id);
        if(!$result){
            throw new Exception('不存在该记录');
        }
        // 更新状态或更新首页推荐
        if($type == 1){
            $data = [
                'status' => intval($status),
            ];
        }else{
            $data = [
                'is_recommend' => intval($status),
            ];
        }
        try{
            $result = $this->model->updateById($id,$data);
        }catch(Exception $e){
            return false;
        }
        return $result;
    }

    public function getNormalByPid($pid = 0, $field='id,name,pid')
    {
        try{
            $categorys = $this->model->getNormalByPid($pid,$field);
        }catch(\Exception $e){
            return [];
        }
        $res = $categorys->toArray();
        return $res;
    }
    
    public function getNormalById($id = 0, $field='id,name,pid')
    {
        try{
            $categorys = $this->model->getNormalById($id,$field);
        }catch(\Exception $e){
            return [];
        }
        $res = $categorys->toArray();
        return $res;
    }

    public function getRecommendCategory()
    {
        try{
            $categorys = $this->model->where(['is_recommend' => 1])->column('id');
        }catch(\Exception $e){
            return [];
        }
        return $categorys;
    }

}
