<?php

namespace app\common\business;

use app\common\model\mysql\Order as OrderModel;
use app\common\business\OrderGoods;
use think\facade\Cache;

class Order extends BusBase
{

    protected $model = null;

    public function __construct()
    {
        $this->model = new OrderModel();
    }

    public function save($data)
    {
        // 获取订单号
        $snowflake = new \Godruoyi\Snowflake\Snowflake;
        $orderId = $snowflake->id();
        // 获取购物车数据
        $cartObj = new Cart();
        $result = $cartObj->lists($data['user_id'],$data['ids']);
        if(!$result){
            return false;
        }
        $newResult = array_map(function($v) use($orderId){
            $v['sku_id'] = $v['id'];
            unset($v['id']);
            $v['order_id'] = $orderId;
            return $v;
        },$result);
        // 组装order数据
        $price = array_sum(array_column($result,'total_price'));
        $orderData = [
            'user_id' => $data['user_id'],
            'total_price' => $price,
            'order_id' => $orderId,
            'address_id' => $data['address_id'],
            'pay_type' => 1,
        ];
        // 增加到order表
        $this->model->startTrans();
        try{
            $id = $this->add($orderData);
            if(!$id){
                return false;
            }
            $orderGoodsResult = (new OrderGoods())->saveAll($newResult);
            // // 减库存
            $skuRes = (new GoodsSku())->updateStock($result);
            // 清除购物车
            (new Cart())->deleteRedis($data['user_id'],$data['ids']);
            $this->model->commit();

            // 增加消息队列处理无效订单
            try{
                Cache::zAdd(config('redis.order_status_key'),time() + config('redis.order_expire'),$orderId);
            }catch(\Exception $e){
                // 监控日志
            }

            return [
                'id' => $orderId,
            ];
        }catch(\Exception $e){
            dd($e->getMessage());
            $this->model->rollback();
            return false;
        }
    }

    // 订单详情
    public function detail($data)
    {
        // 重新组装条件
        $data = [
            'user_id' => $data['user_id'],
            'order_id' => $data['order_id'],
        ];
        // 获取order数据
        $orders = $this->model->getByCondition($data);
        $orders = $orders->toArray();
        // 转换为单条数据 
        $orders = !empty($orders) ? $orders[0] : [];
        if(!$orders){
            return [];
        }
        // 格式化数据
        $orders['id'] = intval($orders['order_id']);
        $orders['consignee_info'] = '山东省 济南市 槐荫区 XXX收';
        // 获取sku数据
        $orderGoods = (new OrderGoods())->getByOrderId($data['order_id']);
        $orders['malls'] = $orderGoods;
        return $orders;
    }

    public function checkOrderStatus()
    {
        // 取出符合条件的数据
        $result = Cache::store('redis')->zRangeByScore('order_status',0,time(),['limit' => [0,10]]);
        if(empty($result)){
            return false;
        }
        try{
            $delRedis = Cache::store('redis')->zRem('order_status',$result[0]);
        }catch(\Exception $e){
            $delRedis =  '';
        }
        if($delRedis){
            echo '删除无效订单'.$result[0].PHP_EOL;
        }else{
            return false;
        }
        return true;
    }


}