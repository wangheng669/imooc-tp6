<?php
/*
 * @Author: your name
 * @Date: 2020-02-05 22:09:10
 * @LastEditTime : 2020-02-13 00:59:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\common\business\Sms.php
 */

namespace app\common\business;

use app\common\lib\ClassArr;
use app\common\lib\Num;

class Sms{

    public static function sendCode($phoneNumber,$len,$type = 'ali')
    {
        $code = Num::getCode($len);
        // $sms = AliSms::sendCode($phoneNumber,$code);

        // $type = ucfirst($type);
        // $class = 'app\common\lib\sms\\'.$type.'Sms';
        // $sms = $class::sendCode($phoneNumber,$code);

        $class = ClassArr::smsClassStat();
        $classObj = ClassArr::initClass($type,$class);
        $sms = $classObj::sendCode($phoneNumber,$code);
        if($sms){
            // 将手机号对应的验证码存入redis
            cache(config('redis.code_pre').$phoneNumber,$code,config('redis.code_expire'));
        }
        return $sms;
    }

}
