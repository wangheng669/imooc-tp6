<?php

namespace app\common\business;

use app\common\model\mysql\Goods as GoodsModel;
use app\common\business\GoodsSku as GoodsSkuBis;
use app\common\business\Category as CategoryBis;
use app\common\lib\Arr;

class Goods extends BusBase
{

    protected $model = null;

    public function __construct()
    {
        $this->model = new GoodsModel();
    }

    public function insertData($data)
    {
        $this->model->startTrans();
        try{
            $data['goods_id'] = $this->add($data);
            // 商品规格表添加
            if($data['goods_specs_type'] == 2){
                $skuData = (new GoodsSkuBis())->saveAll($data);
                // 回写商品表,更新库存,原价,现价,skuid
                if(!$skuData){
                    $stock = array_sum(array_column($skuData,'stock'));
                    $goodsData = [
                        'price' => $skuData[0]['price'],
                        'cost_price' => $skuData[0]['cost_price'],
                        'stock' => $stock,
                        'sku_id' => $skuData[0]['id'],
                    ];
                    $this->model->updateById($data['goods_id'],$goodsData);
                }
            }else{
                // 将单一规格插入sku表
                $skuId = (new GoodsSkuBis())->add($data);
                // 回写商品表
                $goodsData = [
                    'sku_id' => $skuId
                ];
                $this->model->updateById($data['goods_id'],$goodsData);
            }
            $this->model->commit();
        }catch(\Exception $e){
            $this->model->rollback();
            return false;
        }
        return true;
    }

    public function getLists($data,$num)
    {
        try{
            $list = $this->model->getLists($data,$num);
            $result = $list->toArray();
            $result['render'] = $list->render();
        }catch(\Exception $e){
            $result = Arr::getPaginateDefaultData($num);
        }
        return $result;
    }

    public function getRotationChart()
    {
        $where = [
            'is_index_recommend' => 1,
        ];
        $field = 'id,title,big_image as image';
        try{
            $result = $this->model->getNormalGoodsByCondition($where,$field,5);
        }catch(\Exception $e){
            $result = [];
        }
        return $result->toArray();
    }

    public function categoryGoodsRecommend($categoryIds)
    {
        $result = [];
        foreach($categoryIds as $key => $categoryId){
            // 获取分类下的二级分类

            $result[$key]['categorys'] = $this->getGoodsCategory($categoryId);
        }
        foreach($categoryIds as $key => $categoryId){
            $result[$key]['goods'] = $this->getNormalGoodsFindInSetCategoryId($categoryId);
        }
        return $result;
    }

    public function getNormalGoodsFindInSetCategoryId($categoryId)
    {
        $field = 'sku_id as id,recommend_image as image,title,price';
        // 遍历ID获取分类下的商品
        try{
            $result = $this->model->getNormalGoodsFindInSetCategoryId($categoryId,$field,5);
        }catch(\Exception $e){
            $result = [];
        }
        return $result->toArray();
    }

    public function getNormalLists($data,$num = 5,$listorder)
    {
        $field = 'sku_id as id,recommend_image as image,title,price';
        // 遍历ID获取分类下的商品
        try{
            $list = $this->model->getNormalLists($data,$num,$field,$listorder);
            $res = $list->toArray();
            $result = [
                'total_page_num' => isset($res['last_page']) ? $res['last_page'] : 0,
                'count' => isset($res['total']) ? $res['total'] : 0,
                'page' => isset($res['current_page']) ? $res['current_page'] : 0,
                'page_size' => $num,
                'list' => isset($res['data']) ? $res['data'] : [],
            ];
        }catch(\Exception $e){
            $result = [];
        }
        return $result;
    }

    public function getGoodsCategory($id)
    {
        $category = (new CategoryBis())->getNormalById($id,'id as category_id,name,icon');
        $category['list'] = (new CategoryBis())->getNormalByPid($id,'name,id as category_id');
        return $category;
    }

    public function getNormalGoodsById($id)
    {
        try{
            $result = $this->model->getNormalGoodsById($id);
        }catch(\Exception $e){
            $result = [];
        }
        return $result->toArray();
    }
}