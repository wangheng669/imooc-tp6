<?php

namespace app\common\business;

use app\common\model\mysql\OrderGoods as OrderGoodsModel;

class OrderGoods extends BusBase
{

    protected $model = null;

    public function __construct()
    {
        $this->model = new OrderGoodsModel();
    }

    public function saveAll($data)
    {
        try{
            $result = $this->model->saveAll($data);
            return $result->toArray();
        }catch(\Exception $e){
            dd($e->getMessage());
            return false;
        }
    }

    // 根据orderid获取sku数据
    public function getByOrderId($orderId)
    {
        $condition = [
            'order_id' => $orderId
        ];
        $result = $this->model->getByCondition($condition);
        return $result->toArray();
    }

}