<?php

namespace app\common\business;

use app\common\lib\Str;
use app\common\lib\Time;
use app\common\model\mysql\User as UserModel;

class User{

    protected $userObj = null;

    public function __construct()
    {
        $this->userObj = new UserModel();
    }

    public function login($data)
    {
        // 校验验证码
        $redisCode = cache(config('redis.code_pre').$data['phone_number']);
        if(empty($redisCode) || $redisCode != $data['code']){
            throw new \think\Exception('不存在该验证',-1009);
        }
        // 判断表 没有则新增
        $result = $this->userObj->getUserByPhoneNumber($data['phone_number']);
        if(!$result){
            $userData = [
                'username' => $data['phone_number'],
                'type' => $data['type'],
                'status' => config('status.mysql.table_normal'),
            ];
            try {
                $this->userObj->save($userData);
                $userId = $this->userObj->id;
            }catch(\Exception $e){
                throw new \think\Exception('数据库内部异常');
            }
            $userName = $data['phone_number'];
        }else{
            // 更新表
            $userData = [
                'ltype' => 1,
            ];
            $this->userObj->updateById($result['id'],$userData);
            $userId = $result['id'];
            $userName = $result['username'];
        }
        // 生成token
        $token = Str::getLoginToken($data['phone_number']);
        $redisData = [
            'id' => $userId,
            'username' => $userName,
        ];
        $res = cache(config('redis.token_pre').$token,$redisData,Time::userLoginExpiresTime(2));
        return $res ? ['token' => $token, 'username' => $userName] : false;
    }

    // 获取正常用户的信息
    public function getNormalUserByUserId($id)
    {
        $user = $this->userObj->getUserById($id);
        if(!$user || $user->status != config('status.mysql.table_normal')){
            return [];
        }
        return $user->toArray();
    }

    // 通过用户昵称获取正常用户的信息
    public function getNormalUserByUserName($username)
    {
        $user = $this->userObj->getUserByUserName($username);
        if(!$user || $user->status != config('status.mysql.table_normal')){
            return [];
        }
        return $user->toArray();
    }

    // 更新用户数据
    public function update($id,$data,$accessToken)
    {
        $result = $this->getNormalUserByUserId($id);
        // 判断是否存在该用户
        if(!$result){
            throw new \think\Exception('不存在该用户');
        }
        // 根据用户名获取数据
        $userResult = $this->getNormalUserByUserName($data['username']);
        if($userResult && $userResult['id'] != $id){
            throw new \think\Exception('该用户已经存在请重新设置');
        }
        // 更新redis
        $redisData = [
            'id' => $id,
            'username' => $data['username'],
        ];
        cache(config('redis.token_pre').$accessToken,$redisData);
        return $this->userObj->updateById($id,$data);
    }

}