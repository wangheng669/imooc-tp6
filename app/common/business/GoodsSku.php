<?php

namespace app\common\business;

use app\common\model\mysql\GoodsSku as GoodsSkuModel;

class GoodsSku extends BusBase
{

    protected $model = null;

    public function __construct()
    {
        $this->model = new GoodsSkuModel();
    }


    public function saveAll($data)
    {
        foreach($data['skus'] as $value){
            $insertData[] = [
                'goods_id' => $data['goods_id'],
                'specs_value_ids' => $value['propvalnames']['propvalids'],
                'price' => $value['propvalnames']['skuSellPrice'],
                'cost_price' => $value['propvalnames']['skuMarketPrice'],
                'stock' => $value['propvalnames']['skuStock'],
            ];
        }
        $result = $this->model->saveAll($insertData);
        return $result->toArray();
    }

    // 增加单一规格
    public function add($data)
    {
        $skuData = [
            'goods_id' => $data['goods_id'],
            'specs_value_ids' => 0,
            'price' => $data['market_price'],
            'cost_price' => $data['sell_price'],
            'stock' => $data['stock'],
        ];
        $this->model->save($skuData);
        return $this->model->id;
    }

    // 获取商品详情
    public function getNormalSkuAndGoods($id)
    {
        try{
            $result = $this->model->with('goods')->find($id);
        }catch(\Exception $e){
            $result = [];
        }
        return $result->toArray();
    }

    public function getSkuByGoodsId($id)
    {
        $field = 'id,goods_id,specs_value_ids,price,cost_price,stock';
        try{
            $result = $this->model->getSkuByGoodsId($id,$field);
        }catch(\Exception $e){
            $result = [];
        }
        return $result->toArray();
    }

    // 减库存
    public function updateStock($data)
    {
        foreach($data as $value){
            $this->model->decStock($value['id'],$value['num']);
        }
        return true;
    }

    public function getNormalSkuByIds($ids)
    {
        try{
            $result = $this->model->getNormalByIds($ids);
        }catch(\Exception $e){
            $result = [];
        }
        return $result->toArray();
    }

}