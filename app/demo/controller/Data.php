<?php
/*
 * @Author: your name
 * @Date: 2020-02-01 22:35:14
 * @LastEditTime : 2020-02-02 15:13:30
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\controller\Data.php
 */

namespace app\demo\controller;

use app\BaseController;
use app\common\model\mysql\Test;
use think\facade\Db;

class Data extends BaseController
{

    public function index()
    {
        $result = Db::table('test')->select();
        dump($result);
    }

    public function model()
    {
        $result = Test::find(1);
        dump($result->toArray());
    }

    public function error()
    {
        // 输出一个不存在的变量
        echo $a;
        // throw new \think\exception\HttpException(400,'找不到数据');
    }

}