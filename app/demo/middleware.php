<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 18:39:38
 * @LastEditTime : 2020-02-02 18:40:00
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\demo\middleware.php
 */
// 全局中间件定义文件
return [
    // 全局请求缓存
    // \think\middleware\CheckRequestCache::class,
    // 多语言加载
    // \think\middleware\LoadLangPack::class,
    // Session初始化
    app\demo\middleware\Check::class
];
