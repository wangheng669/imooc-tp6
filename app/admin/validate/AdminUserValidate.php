<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 23:39:02
 * @LastEditTime : 2020-02-05 00:14:44
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\validate\AdminUserValidate.php
 */

namespace app\admin\validate;

use think\Validate;

class AdminUserValidate extends Validate
{
    
    protected $rule = [
        'username' => 'require',
        'password' => 'require',
        'captcha' => 'require|checkCaptcha',
    ];

    protected $mssage = [
        'username' => '用户名不能为空',
        'password' => '密码不能为空',
        'captcha' => '验证码不能为空',
    ];

    public function checkCaptcha($value,$rule,$data = [])
    {
        if(!captcha_check($value)){
            return "验证码不正确";
        }
        return true;
    }

}