<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 23:39:02
 * @LastEditTime: 2020-02-19 23:48:15
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\validate\AdminUserValidate.php
 */

namespace app\admin\validate;

class CategoryValidate extends AdminUserValidate
{
    
    protected $rule = [
        'pid' => 'require',
        'name' => 'require',
        'listorder' => 'require',
        'status' => 'require',
    ];

    protected $mssage = [
        'pid.requrie' => '父ID不能为空',
        'name.requrie' => '分类名称不能为空',
        'listorder.requrie' => '排序不能为空',
        'status.requrie' => '状态不能为空',
    ];

    protected $scene = [
        'save' => ['pid','name'],
        'listorder' => ['id','listorder'],
        'status' => ['status'],
    ];

}