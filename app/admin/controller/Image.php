<?php

namespace app\admin\controller;

use think\facade\Filesystem;

class Image extends AdminBase
{

    // 图片上传
    public function upload()
    {
        $file = $this->request->file('file');
        $filename = Filesystem::disk('public')->putFile('image',$file);
        $data = [
            'image' => '/upload/'.$filename,
        ];
        return show(config('status.success'),'OK',$data);
    }
    
    // 编辑器图片上传
    public function layUpload()
    {
        $file = $this->request->file('file');
        $filename = Filesystem::disk('public')->putFile('image',$file);
        $data = [
            'code' => 0,
            'data' => [
                'src' => '/upload/'.$filename,
            ],
        ];
        return json($data);
    }

}