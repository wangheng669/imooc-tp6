<?php
/*
 * @Author: your name
 * @Date: 2020-02-02 20:14:42
 * @LastEditTime : 2020-02-05 00:13:56
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\controller\Login.php
 */

namespace app\admin\controller;

use app\admin\business\AdminUser as BusinessAdminUser;
use app\admin\validate\AdminUserValidate;
use app\common\model\mysql\AdminUser;
use Exception;
use think\facade\View;

class Login extends AdminBase
{

    public function initialize()
    {
        if(!empty($this->isLogin())){
            return $this->redirect(url('index/index'));
        }
    }

    public function index()
    {
        return View::fetch();
    }

    public function md5()
    {
        echo md5('123456'.'wangheng');
    }

    // 登录校验
    public function check()
    {

        // 校验请求方式
        if(!$this->request->isPost()){
            return show(config('status.error'),'请求方式错误');
        }
        // 参数校验

        $username = $this->request->param('username','','trim');
        $password = $this->request->param('password','','trim');
        $captcha = $this->request->param('captcha','','trim');

        $data = [
            'username' => $username,
            'password' => $password,
            'captcha' => $captcha,
        ];
        $validate = new AdminUserValidate();
        if(!$validate->check($data)){
            return show(config('status.error'),$validate->getError());
        }

        // if(empty($username) || empty($password) || empty($captcha)){
        //     return show(config('status.error'),'参数不能为空');
        // }
        // // 校验验证码
        // if(!captcha_check($captcha)){
        //     return show(config('status.error'),'验证码错误'.$captcha);
        // }
        try{
            $result = BusinessAdminUser::login($data);
        }catch(Exception $e){
            return show(config('status.error'),$e->getMessage());
        }
        if(empty($result)){
            return show(config('status.error'),'登录失败');
        }
        return show(config('status.success'),'登录成功');
    }

}