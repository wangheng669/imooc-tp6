<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 22:45:16
 * @LastEditTime : 2020-02-04 22:50:09
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\controller\Logout.php
 */

namespace app\admin\controller;

class Logout extends AdminBase
{

    public function index()
    {
        // 清除session
        session(config('admin.session_admin'),null);
        return redirect(url('login/index'));
    }

}