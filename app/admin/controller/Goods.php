<?php

namespace app\admin\controller;

use app\common\business\Category as CategoryBis;
use app\common\business\Goods as GoodsBis;

class Goods extends AdminBase
{

    public function index()
    {
        $data = [];
        $title = input('param.title');
        $time = input('param.time');
        if(!empty($title)){
            $data['title'] = $title;
        }
        if(!empty($time)){
            $data['time'] = explode(' - ',$time);
        }
        $goods = (new GoodsBis())->getLists($data,5);
        return view('',[
            'title' => $title,
            'time' => $time,
            'goods' => $goods,
        ]);
    }

    public function add()
    {
        return view();
    }

    public function edit()
    {
        $id = input('param.id',0,'intval');
        // 获取商品信息
        $goods = (new GoodsBis())->getNormalGoodsById($id);
        $categoryIds = explode(',',$goods['category_path_id']);
        $category_path = '';
        foreach($categoryIds as $k => $categoryId){
            $result = (new CategoryBis())->getNormalById($categoryId);
            if($k != 2){
                $str = '->';
            }else{
                $str = '';
            }
            $category_path .= $result['name'].$str;
        }
        return view('',[
            'goods' => $goods,
            'category_path' => $category_path,
        ]);
    }

    public function dialog()
    {
        return view();
    }

    // 保存商品
    public function save()
    {
        // 获取数据
        $data = input('param.');
        // 校验token
        if(!$this->request->checkToken('__token__')){
            return show(config('status.error'),'非法请求');
        }
        // 校验数据
        $data['category_path_id'] = $data['category_id'];
        $cateogryIds = explode(',',$data['category_id']);
        $data['category_id'] = end($cateogryIds);
        $data['price'] = $data['market_price'];
        $data['cost_price'] = $data['sell_price'];
        // 商品基本表添加
        $result = (new GoodsBis())->insertData($data);
        if(!$result){
            return show(config('status.error'),'商品新增失败');
        }
        return show(config('status.success'),'商品新增成功');
    }


}