<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 22:31:39
 * @LastEditTime : 2020-02-04 23:18:57
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\controller\AdminBase.php
 */

namespace app\admin\controller;

use app\BaseController;
use think\exception\HttpResponseException;

class AdminBase extends BaseController
{

    protected $adminUser = null;

    // 基类方法
    public function initialize()
    {
        parent::initialize();

        // 未登录则跳转登录页面
        // if(empty($this->isLogin())){
        //     return $this->redirect(url('login/index'),302);
        // }

    }

    // 验证是否登录
    public function isLogin()
    {
        $this->adminUser = session(config('admin.session_admin'));
        if(empty($this->adminUser)){
            return false;
        }
        return true;
    }

    public function redirect(...$args)
    {
        throw new HttpResponseException(redirect(...$args));
    }

}