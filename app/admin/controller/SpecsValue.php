<?php

namespace app\admin\controller;

use app\common\business\SpecsValue as SpecsValueBus;

class SpecsValue
{

    public function save()
    {
        $specsId = input('param.specs_id',0,'intval');
        $name = input('param.name','');
        $data = [
            'specs_id' => $specsId,
            'name' => $name,
            'operate_user' => 'wangheng',
        ];
        $id = (new SpecsValueBus())->add($data);
        if(!$id){
            return show(config('status.error'),'error');
        }
        return show(config('status.success'),'OK',['id' => $id]);
    }

    public function getBySpecsId()
    {
        $specsId = input('param.specs_id',0,'intval');
        if(!$specsId){
            return show(config('status.error'),'没有数据');
        }
        $data = (new SpecsValueBus())->getBySpecsId($specsId);
        return show(config('status.success'),'OK',$data);
    }

    public function del()
    {
        $specsId = input('param.specs_id',0,'intval');
        $res = (new SpecsValueBus())->delSpecs($specsId);
        if(!$res){
            return show(config('status.error'),'error');
        }
        return show(config('status.success'),'OK');
    }

}