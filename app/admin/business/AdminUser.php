<?php
/*
 * @Author: your name
 * @Date: 2020-02-04 23:57:41
 * @LastEditTime : 2020-02-05 00:04:29
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \imooc-tp6\app\admin\business\AdminUser.php
 */

namespace app\admin\business;

use app\common\model\mysql\AdminUser as AdminUserModel;
use Exception;

class AdminUser{

    public static function login($data)
    {
        try{
            $adminUserObj = new AdminUserModel();
            $adminUser = self::getAdminUserByUsername($data['username']);

            if(empty($adminUser)){
                throw new Exception('不存在用户');
            }

            // 判断密码是否正确
            if($adminUser['password'] != md5($data['password'].'wangheng')){
                throw new Exception('密码错误');
            }
    
            // 更新用户数据
            $updateData = [
                'last_login_time' => time(),
                'last_login_ip' => request()->ip(),
            ];
    
            $result = $adminUserObj->updateById($adminUser['id'],$updateData);
            if(!$result){
                throw new Exception('登录失败');
            }
        }catch(Exception $e){
            throw $e;
            // 日志记录
            throw new Exception('内部异常,登录失败');
        }
        // 保存session
        session(config('admin.session_admin'),$adminUser);
        return true;
    }

    public static function getAdminUserByUsername($username)
    {
        $adminUserObj = new AdminUserModel();
        $adminUser = $adminUserObj->getAdminUserByUsername($username);

        // 判断用户是否存在
        if(empty($adminUser) && $adminUser->status != config('status.mysql.table_normal')){
            return false;
        }
        
        // 转换为数组
        $adminUser = $adminUser->toArray();
        return $adminUser;
    }
    

}
