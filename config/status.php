<?php
/*
 * @Author: your name
 * @Date: 2020-02-01 22:16:49
 * @LastEditTime : 2020-02-03 22:17:32
 * @LastEditors  : Please set LastEditors
 * @Description: 业务状态码
 * @FilePath: \imooc-tp6\config\status.php
 */

return [
    'success' => 1,
    'error' => 0,
    'not_login' => -1,
    'user_is_register' => -2,
    'action_not_found' => -3,
    'controller_not_found' => -4,
    'mysql' => [
        'table_normal' => 1,
        'table_pedding' => 0,
        'table_delete' => 99,
    ],
];