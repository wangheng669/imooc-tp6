<?php
//git webhook 自动部署脚本
//项目存放物理路径,第一次clone时,必须保证该目录为空
$savePath = "/www/wwwroot/imooc-tp6/";
$gitPath  = "https://gitee.com/wangheng669/imooc-tp6.git";//代码仓库
$email = "2658803113@qq.com";//用户仓库邮箱
$name  = "wangheng669";//仓库用户名,一般和邮箱一致即可
$requestBody = file_get_contents("php://input");
if (empty($requestBody)) {
    die('send fail');
}
//解析Git服务器通知过来的JSON信息
$content = json_decode($requestBody, true);
//若是主分支且提交数大于0
$shell = "cd {$savePath} && git pull";    
$res = shell_exec($shell);